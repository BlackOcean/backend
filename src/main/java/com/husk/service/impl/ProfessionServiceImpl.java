/**
 * 
 */
package com.husk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.husk.model.Profession;
import com.husk.repository.ParentRepository;
import com.husk.repository.ProfessionRespository;

/**
 * @author akshaysolanki
 *
 */
@Service
public class ProfessionServiceImpl extends ParentRepository<Profession, Integer> {

	@Autowired
	private ProfessionRespository professionRespository;

	@Override
	protected JpaRepository<Profession, Integer> getJPARepository() {
		return professionRespository;
	}

}
