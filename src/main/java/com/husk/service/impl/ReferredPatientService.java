package com.husk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.husk.model.ReferredPatient;
import com.husk.repository.ParentRepository;
import com.husk.repository.ReferredPatientRepsoitory;

@Service
public class ReferredPatientService extends ParentRepository<ReferredPatient, Integer> {

	@Autowired
	private ReferredPatientRepsoitory repo;

	@Override
	protected JpaRepository<ReferredPatient, Integer> getJPARepository() {
		return repo;
	}

}
