/**
 * 
 */
package com.husk.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.husk.model.User;
import com.husk.repository.ParentRepository;
import com.husk.repository.UserRepository;

/**
 * @author akshaysolanki
 *
 */
@Service
public class UserServiceImpl extends ParentRepository<User, Integer> {
	@Autowired
	private UserRepository userRepository;

	public byte[] getUserImage(Integer id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isPresent())
			return user.get().getImage();
		return null;
	}

	@Override
	protected JpaRepository<User, Integer> getJPARepository() {
		return userRepository;
	}

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

}
