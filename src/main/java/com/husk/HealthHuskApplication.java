package com.husk;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.husk.utility.HuskConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@SpringBootApplication
@RestController
public class HealthHuskApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthHuskApplication.class, args);
	}
	
	@ApiOperation(value = "save User")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@GetMapping("/")
	public String saveUser() throws IOException {

		return "ok";

	}

}
