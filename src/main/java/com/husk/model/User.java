/**
 * 
 */
package com.husk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.husk.generic.utilities.ParentEntity;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author akshaysolanki
 *
 */
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Setter
public class User extends ParentEntity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	private String firstName;
	private String lastName;
	@Column(unique = true, nullable = false)
	private String email;
	private Long mobileNumber;
	private String address;
	private Integer pincode;
	private String city;
	private String details;
	private byte[] image;
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;
	@Transient
	private MultipartFile file;
	@OneToOne
	@JoinColumn(name = "profession_id", referencedColumnName = "id")
	private Profession profession;
	@Column(columnDefinition = "boolean default false")
	private Boolean isDoctor;
}
