/**
 * 
 */
package com.husk.model;

import javax.persistence.Entity;

import com.husk.generic.utilities.ParentEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author akshaysolanki
 *
 */
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Profession extends ParentEntity<Integer> {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	private String name;

}
