package com.husk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.husk.generic.utilities.ParentEntity;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReferredPatient extends ParentEntity<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull
	private String firstName;
	private String lastName;
	@Column(unique = true, nullable = false)
	private String email;
	private Long mobileNumber;
	private String address;
	private Integer pincode;
	private String city;
	private String details;
	@OneToOne
	@JoinColumn(name = "referred_by", referencedColumnName = "id")
	private User user;

}
