/**
 * 
 */
package com.husk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.husk.model.ReferredPatient;

/**
 * @author akshaysolanki
 *
 */
@Repository
public interface ReferredPatientRepsoitory extends JpaRepository<ReferredPatient, Integer> {

}
