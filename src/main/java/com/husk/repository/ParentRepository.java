package com.husk.repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.husk.generic.utilities.ParentEntity;

@NoRepositoryBean
public abstract class ParentRepository<T extends ParentEntity, E> implements JpaRepository<T, E> {
	protected abstract JpaRepository<T, E> getJPARepository();

	public List<T> getAllNonDeleted() {

		List<T> returnedValues = getJPARepository().findAll();
		List<T> nonDeletedResults = returnedValues.stream().filter(x -> x.getSoftDelete() == false)
				.collect(Collectors.toList());
		return nonDeletedResults;
	}

	public void softdeleteUser(E id) {
		Optional<T> returnedObject = getJPARepository().findById(id);
		if (returnedObject.isPresent()) {
			returnedObject.get().setSoftDelete(true);
			getJPARepository().save(returnedObject.get());

		}
	}

	@Override
	public <S extends T> Optional<S> findOne(Example<S> example) {
		return getJPARepository().findOne(example);
	}

	@Override
	public <S extends T> Page<S> findAll(Example<S> example, Pageable pageable) {
		return getJPARepository().findAll(example, pageable);
	}

	@Override
	public <S extends T> long count(Example<S> example) {
		return getJPARepository().count();
	}

	@Override
	public <S extends T> boolean exists(Example<S> example) {
		return getJPARepository().exists(example);
	}

	@Override
	public Page<T> findAll(Pageable pageable) {
		return getJPARepository().findAll(pageable);
	}

	@Override
	public <S extends T> S save(S entity) {
		return getJPARepository().save(entity);
	}

	@Override
	public Optional<T> findById(E id) {
		return getJPARepository().findById(id);
	}

	@Override
	public boolean existsById(E id) {
		return getJPARepository().existsById(id);
	}

	@Override
	public long count() {
		return getJPARepository().count();
	}

	@Override
	public void deleteById(E id) {
		getJPARepository().deleteById(id);

	}

	@Override
	public void delete(T entity) {
		getJPARepository().delete(entity);

	}

	@Override
	public void deleteAll(Iterable<? extends T> entities) {
		getJPARepository().deleteAll(entities);

	}

	@Override
	public void deleteAll() {
		getJPARepository().deleteAll();
	}

	@Override
	public List<T> findAll() {
		return getJPARepository().findAll();
	}

	@Override
	public List<T> findAll(Sort sort) {
		return getJPARepository().findAll(sort);
	}

	@Override
	public List<T> findAllById(Iterable<E> ids) {
		return getJPARepository().findAllById(ids);
	}

	@Override
	public <S extends T> List<S> saveAll(Iterable<S> entities) {
		return getJPARepository().saveAll(entities);
	}

	@Override
	public void flush() {
		getJPARepository().flush();
	}

	@Override
	public <S extends T> S saveAndFlush(S entity) {
		return getJPARepository().saveAndFlush(entity);
	}

	@Override
	public void deleteInBatch(Iterable<T> entities) {
		getJPARepository().deleteInBatch(entities);
		;
	}

	@Override
	public void deleteAllInBatch() {
		getJPARepository().deleteAllInBatch();
	}

	@Override
	public T getOne(E id) {
		return getJPARepository().getOne(id);
	}

	@Override
	public <S extends T> List<S> findAll(Example<S> example) {
		return getJPARepository().findAll(example);
	}

	@Override
	public <S extends T> List<S> findAll(Example<S> example, Sort sort) {
		return getJPARepository().findAll(example, sort);
	}

}
