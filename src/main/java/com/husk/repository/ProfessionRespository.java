package com.husk.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.husk.model.Profession;

public interface ProfessionRespository extends JpaRepository<Profession, Integer> {

}
