/**
 * 
 */
package com.husk.generic.utilities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author akshaysolanki
 *
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@Setter
public abstract class ParentEntity<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private T id;
	@Column(columnDefinition = "boolean default false")
	private Boolean softDelete;

}
