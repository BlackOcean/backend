package com.husk.utility;

public final class HuskConstants {
	public static final String ERROR_401 = "You are not authorized to view the resource";
	public static final String ERROR_403 = "Accessing the resource you were trying to reach is forbidden";
	public static final String ERROR_404 = "The resource you were trying to reach is not found";
	public static final String BASE_URL = "/husk/backend";

	private HuskConstants() throws IllegalAccessException {
		throw new IllegalAccessException("CANNOT BE INSTANTIATED");
	}

}
