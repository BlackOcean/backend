package com.husk.security;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

/**
 * @author To The New
 *
 */
@Component
public class TokenAuthenticationService {

	private String secret = "VMS";
	private String headerString = "Authorization";

	/**
	 * returns authenticated user
	 */
	public Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(headerString);
		if (token != null) {
			// parse the token.
			try {
				String username = Jwts.parser().setSigningKey(Base64.getEncoder().encodeToString(secret.getBytes()))
						.parseClaimsJws(token).getBody().getSubject();
				if (username != null) // we managed to retrieve a user

				{
					return new AuthenticatedUser(username);
				}
			} catch (ExpiredJwtException ex) {
				request.setAttribute("expired", ex.getMessage());
			}

		}
		return null;
	}


}
