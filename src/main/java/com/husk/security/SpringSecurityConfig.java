/**
 * 
 */
package com.husk.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import com.husk.utility.SwaggerConfig;


/**
 * @author To The New
 *
 */
@Configuration
@Import(SwaggerConfig.class)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer

{

	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor());
	}

	// for CORS
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedMethods("GET", "POST", "OPTIONS").exposedHeaders("Authorization")
				.allowedOrigins("*");

	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// disable caching
		http.headers().cacheControl();
		http.cors().and().csrf().disable() // disable csrf for our requests.
				.authorizeRequests().antMatchers("/").permitAll()
				.anyRequest().authenticated().and()
				// And filter other requests to check the presence of JWT in header
				.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
				"/configuration/security", "/swagger-ui.html", "/webjars/**", "/actuator/info");
	}

//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//
//		http.headers().cacheControl();
//		http.csrf().disable().authorizeRequests().antMatchers("/health").permitAll().antMatchers("/v2/**").permitAll()
//				.antMatchers("/docs/**").permitAll().antMatchers("/api/**").authenticated().antMatchers("/**")
//				.permitAll().antMatchers("/axfood/axfood-product-scan/**").authenticated() // needs to be the last
//																							// matcher, otherwise all
//																							// matchers following it
//																							// would never
//				// be reached
//				.anyRequest().authenticated().and()
//				.addFilterBefore(awsCognitoJwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
//	}

}
