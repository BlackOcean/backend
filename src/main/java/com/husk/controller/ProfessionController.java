/**
 * 
 */
package com.husk.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.husk.model.Profession;
import com.husk.service.impl.ProfessionServiceImpl;
import com.husk.utility.HuskConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author akshaysolanki
 *
 */
@RestController
@RequestMapping(value = HuskConstants.BASE_URL)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProfessionController {

	@Autowired
	private ProfessionServiceImpl professionService;

	@ApiOperation(value = "save User")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@PostMapping("/save/profession")
	public List<Profession> saveUser(@RequestBody List<Profession> professions) throws IOException {

		return professionService.saveAll(professions);

	}

	@ApiOperation(value = "getAllUsers")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@GetMapping("/getAll/professions")
	public List<Profession> getAllUsers() {
		return professionService.getAllNonDeleted();

	}

}
