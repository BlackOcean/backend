/**
 * 
 */
package com.husk.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.husk.model.User;
import com.husk.service.impl.UserServiceImpl;
import com.husk.utility.HuskConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author akshaysolanki
 *
 */
@RestController
@RequestMapping(value = HuskConstants.BASE_URL)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
	@Autowired
	private UserServiceImpl userService;

	private PasswordEncoder encoder = new BCryptPasswordEncoder();

	@ApiOperation(value = "save User")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@PostMapping("/save/user")
	public User saveUser(@RequestBody User user) throws IOException {
		User returnedUser = userService.findByEmail(user.getEmail());
		if (null != returnedUser) {
			user.setPassword(returnedUser.getPassword());
		}
		user.setPassword(encoder.encode(user.getPassword()));
		byte[] targetArray = user.getFile().getBytes();
		user.setImage(targetArray);
		return userService.save(user);

	}

	@ApiOperation(value = "getAllUsers")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@GetMapping("/getAll/users")
	public List<User> getAllUsers() {
		return userService.getAllNonDeleted();

	}

	@GetMapping(value = "/getImage/user/{id}")
	@ApiOperation(value = "get user details with user id")
	public byte[] getUserImage(@PathVariable Integer id) {
		return userService.getUserImage(id);
	}

	@DeleteMapping(value = "/delete/user/{id}")
	@ApiOperation(value = "delete user details with user id")
	public void deleteUser(@PathVariable Integer id) {
		userService.softdeleteUser(id);
	}
}
