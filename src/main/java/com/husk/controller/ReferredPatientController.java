/**
 * 
 */
package com.husk.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.husk.model.ReferredPatient;
import com.husk.service.impl.ReferredPatientService;
import com.husk.utility.HuskConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author akshaysolanki
 *
 */
@RestController
@RequestMapping(value = HuskConstants.BASE_URL)
@Api(value = "health husk details")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ReferredPatientController {
	@Autowired
	private ReferredPatientService referredPatientService;

	@ApiOperation(value = "save User")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@PostMapping("/save/referred/patient")
	public ReferredPatient saveUser(@RequestBody ReferredPatient patient) throws IOException {

		return referredPatientService.save(patient);

	}

	@ApiOperation(value = "getAllReferredPatients")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched"),
			@ApiResponse(code = 401, message = HuskConstants.ERROR_401),
			@ApiResponse(code = 403, message = HuskConstants.ERROR_403),
			@ApiResponse(code = 404, message = HuskConstants.ERROR_404) })
	@GetMapping("/getAll/referred/patients")
	public List<ReferredPatient> getAllUsers() {
		return referredPatientService.getAllNonDeleted();

	}

	@DeleteMapping(value = "/delete/referred/patient/{id}")
	@ApiOperation(value = "delete user details with user id")
	public void deleteUser(@PathVariable Integer id) {
		referredPatientService.softdeleteUser(id);
	}
}
