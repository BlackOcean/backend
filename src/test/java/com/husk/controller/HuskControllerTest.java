package com.husk.controller;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.husk.repository.UserRepository;
import com.husk.service.impl.UserServiceImpl;

@SpringBootTest
public class HuskControllerTest {
	@InjectMocks
	private UserServiceImpl userServiceImpl;
	@Mock
	private UserRepository userRepository;

	@Test
	void findByEmailTest() {
		Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(null);
		userServiceImpl.findByEmail("test");
	}
}
